<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ArraySum extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:array_sum {array}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sum = 0;
        $array = json_decode($this->argument('array'));

        if(!is_array($array)){
            return $this->error('Is not an array');
        }

        foreach($array as $key => $value)
        {
            if(is_array($value)){
                $sum = $sum +  $this->sumRecursive($value);
            }
            else{
                $sum = $sum + $value;
            }

        }

        echo $sum;
    }

    public function sumRecursive($array){

        $sum = 0;

        foreach($array as $key => $value)
        {
            if(is_array($value)){
                $sum = $sum +  $this->sumRecursive($value);
            }
            else{
                $sum = $sum + $value;
            }
        }

        return $sum;
    }
}
