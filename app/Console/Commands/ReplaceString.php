<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReplaceString extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:string_replace {replace} {str*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $str = '';
        $replace = explode('_',$this->argument('replace'));
        $string = $this->argument('str');
        foreach($replace as $key => $value){
            if($value[0] === "{" && $value[strlen($value) - 1] === "}")
            {
                $index = $value[1];
                $str = $str == '' ? $str . $string[$index] : $str . '_' . $string[$index];
            }
            else{
                $index = $value[1];
                $str = $str == '' ? $str . $value: $str . '_' . $value;
            }

        }
        dd($str);
        return 0;
    }
}
